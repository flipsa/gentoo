#!/bin/sh

# Version: 0.2

# Changelog:
#
# Bugfix: fixed 'sort' command (-V makes sure '10' comes after '9' and not after '1')

# Your kernel sources directory. Usually "/usr/src"
KERNEL_DIR="/usr/src"

if [[ -d "$KERNEL_DIR" ]]
then
	# The current kernel directory (usually symlinked to /usr/src/linux; if your setup is different this will break!)
	CURRENT_VERSION=$(echo $(cd "$KERNEL_DIR"/linux && pwd -P) | cut -d "/" -f4 | cut -d "-" -f2-3) > /dev/null 2>&1

	# The latest kernel available in $KERNEL_DIR (expects kernels to be named 'linux-[version string]', e.g 'linux-4.15.0-gentoo')
	LATEST_VERSION=$(ls "$KERNEL_DIR" | grep linux | sort -rV | head -n1 | cut -d "-" -f2-3) > /dev/null 2>&1
else
	echo "ERROR! Either the KERNEL_DIR variable in the beginning of the script is not set, or your kernel naming scheme is non-standard. Exiting ..."
	exit 1
fi

if [[ ! -z "$CURRENT_VERSION" ]] && [[ ! -z "$LATEST_VERSION" ]]
then
	echo ""
	echo "This script will build a new kernel from source."
	echo ""
	echo "Current kernel: "$KERNEL_DIR"/linux-"$CURRENT_VERSION""
	echo ""
	echo "Latest kernel: "$KERNEL_DIR"/linux-"$LATEST_VERSION""
	echo ""
	echo "Do you want to build a new kernel for linux-"$LATEST_VERSION"???"
	echo ""
	echo "Type (Y)es to continue or (N)o to cancel"
	echo ""

	read -p ">> " CONTINUE
	case $CONTINUE in

		y|Y)

			echo ""
			echo "Do you want to copy your old config file from $KERNEL_DIR/$CURRENT_VERSION to $KERNEL_DIR/$LATEST_VERSION ?"
			echo ""
			echo "(Y) or (N) ?"
			echo ""

			read -p ">> " COPY_OLD_CONF
			case $COPY_OLD_CONF in

				y|Y)
					# Copy the old kernel config file from current kernel to latest kernel directory
					echo ""
					echo "Copying previous config and selecting default values for any new kernel options"
					cp -a -i $KERNEL_DIR/linux-$CURRENT_VERSION/.config $KERNEL_DIR/linux-$LATEST_VERSION/.config
				;;

				n|N)
					echo ""
					echo "You will have to configure the options manually in the next step or your kernel might not boot / work properly!"
				;;

				*)
					echo "WRONG KEY..."
					exit 1
				;;

			esac

			# Change to the directory of the latest kernel
			cd $KERNEL_DIR/linux-$LATEST_VERSION

			echo ""
			echo "How do you want to configure the new kernel?"
			echo ""
			echo "'make oldconfig' use (d)efault for new options"
			echo "'make oldconfig' and (a)sk about new options"
			echo "'make menuconfig' for (m)anual configuration"
			echo "You can also (c)ancel if you need to"
			echo ""

			read -p ">> " MAKE_CONF
			case $MAKE_CONF in
				d)
					# make oldconfig with default options
					echo "Using previous config with defaults for new options..."
					yes "" | make oldconfig
				;;
				a)
					# make oldconfig and ask
					echo "Using previous config and will ask for new options..."
					make oldconfig
				;;
				m)
					# Choose new kernel options
					echo "Manually configuring kernel..."
					make menuconfig
				;;
				c)
					echo ""
					echo "OK, cancelling..."
					exit 1
				;;

				*)
					echo "WRONG KEY..."
					exit 1
				;;
			esac

			echo ""
			echo "OK, building kernel and modules..."

			# Set correct ccache dir and add it to our $PATH
			export CCACHE_DIR="/var/tmp/ccache"
			export PATH="/usr/lib/ccache/bin:${PATH}"

			# Compiler jobs (CPU cores + 1)
			let JOBS=$(nproc)+1
			make -j$JOBS && make modules_install

			echo ""
			echo "Building finished, now copying to /boot"
			mount /boot
			cp .config /boot/config-$LATEST_VERSION
			cp arch/x86_64/boot/bzImage /boot/kernel-$LATEST_VERSION

			echo ""
			echo "Copying finished, now building initramfs"
			dracut -a crypt -f /boot/initramfs-$LATEST_VERSION.img $LATEST_VERSION

			echo ""
			echo "Writing new GRUB config file"
			grub-mkconfig -o /boot/grub/grub.cfg
			#echo "kernel /boot/$VERSION root=/dev/sda6 pcie_aspm=force i915.i915_enable_rc6=1 video=vesafb:1600x900-32@60 splash=verbose,fadein,fadeout,theme:natural_gentoo console=tty1" >> /boot/grub/grub.conf
			umount /boot

			# Delete the softlink to the old kernel and make one for the new kernel
			rm "$KERNEL_DIR"/linux
			ln -s "$KERNEL_DIR"/linux-"$LATEST_VERSION" "$KERNEL_DIR"/linux

			# Rebuilding external modules matching the new kernel
			echo ""
			echo "Rebuilding external modules for new kernel"
			emerge @module-rebuild -v
		;;

		n|N)

			echo ""
			echo "OK, nevermind then..."
			exit 0

		;;

		*)
			echo "WRONG KEY..."
			exit 1
		;;

	esac

else
	echo "Failed the variable check. Boohoo."
	exit 1
fi
