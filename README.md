# Gentoo Scripts

## Kernel Update
Dependencies:
- dracut

Optional dependencies:
- cryptsetup & kernel crypto options (if full-disk encryption is desired)

## System Update
Optional dependencies:
- esearch
- layman

