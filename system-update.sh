#!/bin/sh

# Sync layman and the regular portage tree
if [ -x $(which layman) ]
then
	layman -s ALL
fi

if [ -x $(which esync) ]
then
	esync
else
	emerge --sync
fi

# emerge world
emerge -uDUtva --keep-going @world && \

# Clean up afterwards
emerge --depclean -av && \

# Check for broken libs
revdep-rebuild && \

# Clean up distfiles
rm -rf /usr/portage/distfiles/* && \

# Show the news
eselect news list

